/* eslint-disable jsx-a11y/anchor-is-valid */
import styled from "@emotion/styled";
import { CacheProvider } from '@emotion/react'
import { cache } from '@emotion/css'
import { createTheme, Theme, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";

// icons
import SupportAgentIcon from '@mui/icons-material/SupportAgent';
import ScreenSearchDesktopIcon from '@mui/icons-material/ScreenSearchDesktop';
import SettingsIcon from '@mui/icons-material/Settings';
import ExpandIcon from '@mui/icons-material/ExpandMore';
import LogoutIcon from '@mui/icons-material/Logout';
import MenuIcon from '@mui/icons-material/Menu';
import CloseIcon from '@mui/icons-material/Close';
import SearchIcon from '@mui/icons-material/Search';

import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Filler,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';
import { ThemeProvider } from "@emotion/react";
import { Box } from "@mui/system";

const appleData = require('./data/appl.json');
const teslaData = require('./data/tsla34.json');
const googleData = require('./data/gogl34.json');
const microsoftData = require('./data/msft34.json');

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Filler,
  Legend
);

export const overallChartOptions = {
  responsive: true,
  maintainAspectRatio: false,
  canvas: {
    parentNode: {
      style: {
        height: '128px'
      }
    }
  },
  elements: {
    line: {
      tension: 0.4
    },
  },
  plugins: {
    legend: {
      position: 'top' as const,
      labels: {
        color: 'white'
      }
    },
  },
  scales: {
    y: {
      grid: {
        color: 'rgba(255, 255, 255, 0.4)'
      },
      ticks: {
        color: "white",
        stepSize: 2,
        font: {
          size: 13,
        },
      }
    },
    x: {
      grid: {
        display: false,
      },
      ticks: {
        color: "white",
        font: {
          size: 13
        },
      }
    }
  }
};

const overallChartLabels = ['March', 'April', 'May', 'June', 'July', 'August'];

export const overallChartData = {
  labels: overallChartLabels,
  responsive: true,
  datasets: [
    {
      fill: true,
      label: 'Your Wallet',
      data: [ 
        1.7737008054415933,
        2.8289616149949905,
        1.3776232537601825,
        2.9341981838132973,
        3.012199824944603,
        2.158463450457723
      ],
      borderColor: '#fff',
      backgroundColor: '#fff',
    },
    {
      fill: true,
      label: 'Dow Jones',
      data: [
        7.352299647673178,
        4.8564259295408339,
        6.120534192222575,
        3.8512948832462581,
        9.36618981693298,
        6.083747728380093,
      ],
      borderColor: '#333565',
      backgroundColor: '#333565',
    },
  ],
};

export const stockChartOptions = {
  responsive: true,
  elements: {
    line: {
      tension: 0.4
    },
  },
  canvas: {
    parentNode: {
      style: {
        width: '500px'
      }
    }
  },
  plugins: {
    legend: {
      position: 'bottom' as const,
      labels: {
        color: 'black'
      }
    },
  },
  scales: {
    y: {
      ticks: {
        color: "#A3A3A3",
        stepSize: 1,
        font: {
          size: 13,
        },
      }
    },
    x: {
      grid: {
        display: false,
      },
      ticks: {
        color: "#A3A3A3",
        font: {
          size: 13
        },
      }
    }
  },
};

const stockChartDayLabels = ['08/15', '08/16', '08/17', '08/18', '08/19', '08/20'];
const stockChartWeekLabels = ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5', 'Week 6'];

export const stockChartDayData = {
  labels: stockChartDayLabels,
  responsive: true,
  datasets: [
    {
      fill: false,
      label: 'Your Wallet',
      data: [],
      borderColor: '#293456',
      backgroundColor: '##293456',
    },
  ],
};

export const stockChartWeekData = {
  labels: stockChartWeekLabels,
  responsive: true,
  datasets: [
    {
      fill: false,
      label: 'Your Wallet',
      data: [],
      borderColor: '#293456',
      backgroundColor: '##293456',
    },
  ],
};

const minWidth768pxQuery = `@media (min-width: 768px)`;
const maxWidth768pxQuery = `@media (max-width: 768px)`;

const minWidth576pxQuery = `@media (min-width: 576px)`;
const maxWidth576pxQuery = `@media (max-width: 576px)`;

const AppContainer = styled('section')(() => ({
  display: 'flex',
}))

const SideMenu = styled('aside', { shouldForwardProp: (prop) => prop !== "isOpen" })<{ isOpen?: boolean; }>(({ theme, isOpen }) => ({
  position: 'relative',
  left: '0',
  top: '0',
  bottom: '0',
  width: '310px',
  background: '#293456',
  height: 'calc(100vh - 46px)',
  maxHeight: '100vh',
  color: '#fff',
  padding: '23px',

  [maxWidth768pxQuery]: {
    left: isOpen ? '0px' : `-${window.innerWidth + 100}px`,
    width: `${window.innerWidth - 75}px`,
    height: 'calc(100vh - 50px)',
    transition: 'left .2s',
    zIndex: '999',
    maxWidth: '100%'
  },
}))

const MainContent = styled('main')(({ theme }) => ({
  width: 'calc(100% - 390px)',
  background: '#FAFAFA',
  position: 'relative',
  color: '#fff',
  padding: '40px',
  paddingTop: '67px',
  paddingBottom: '0',
  height: 'calc(100vh - 67px)',
  maxHeight: '100vh',
  overflow: 'auto',

  [maxWidth768pxQuery]: {
    position: 'absolute',
    left: '0',
    width: '100%',
    maxWidth: '100%',
    padding: '0',
    height: '100vh'
  },
}))

const Background = styled('div')(() => ({
  width: '100%',
  height: '500px',
  background: '#897cff',
  position: 'absolute',
  left: '0',
  top: '0',
  right: '0',
}))

const Container = styled('section')(() => ({
  position: 'relative',
  maxWidth: '1050px',
  margin: '0 auto'
}))

const UserContainer = styled('section')(() => ({
  display: 'flex',
  alignItems: 'center',
  gap: '12px',
  marginBottom: '26px',
  cursor: 'pointer'
}))

const UserImage = styled('div')(() => ({
  width: '74px',
  height: '74px',
  background: '#D9D9D9',
  borderRadius: '50%'
}))

const UserName = styled(Typography)(({ theme }) => ({
  fontFamily: 'Arial',
  fontStyle: 'normal',
  fontWeight: '400',
  fontSize: '20px',
  lineHeight: '23px',

  [maxWidth768pxQuery]: {
    width: '90px',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap'
  },
}))

const UserTitle = styled(Typography)(() => ({
  fontFamily: 'Arial',
  fontStyle: 'normal',
  fontWeight: '400',
  fontSize: '14px',
  lineHeight: '16px',
  color: '#CBCBCB'
}))

const MenuItemList = styled('ul')(({ theme }) => ({
  [maxWidth768pxQuery]: {
    'ul:nth-child(4) li:nth-child(2)': {
      display: 'none'
    },
  }
}))

const MenuItem = styled('li')(({ theme }) => ({
  fontFamily: 'Arial',
  fontStyle: 'normal',
  fontWeight: '400',
  fontSize: '16px',
  lineHeight: '19px',
  textTransform: 'uppercase',
  margin: '0 -20px',
  padding: '25px 23px',
  paddingLeft: '40px',
  borderBottom: '1px solid #181C28',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-start',
  gap: '14.5px',
  position: 'relative',
  '&:hover': {
     cursor: 'pointer'
  },
  '&:first-child': {
    borderTop: '1px solid #181C28'
  },

  [minWidth768pxQuery]: {
    '&:nth-child(5)': {
      pointerEvents: 'none',
      cursor: 'pointer'
    },
  },
}))

const LogoutSection = styled('footer')(() => ({
  position: 'absolute',
  left: '0',
  right: '0',
  bottom: '0',
  borderTop: '1px solid #181C28',
  padding: '25px 23px',
  fontFamily: 'Arial',
  fontStyle: 'normal',
  fontWeight: '400',
  fontSize: '16px',
  lineHeight: '19px',
  textTransform: 'uppercase',
  display: 'flex',
  alignItems: 'center',
  gap: '17px',
  '&:hover': {
    cursor: 'pointer'
  },
}))

const SubMenuItemsList = styled('ul', { shouldForwardProp: (prop) => prop !== "isOpen" })<{ isOpen?: boolean; }>(({ isOpen }) => ({
  height: isOpen ? '144px' : '0px',
  overflow: 'hidden',
  transition: 'height .3s',
  background: '#20263A',
  margin: '0 -23px',
}))

const SubMenuItem = styled('li')(({ theme }) => ({
  padding: '14px 60px',
  fontFamily: 'Arial',
  fontStyle: 'normal',
  fontWeight: '400',
  fontSize: '16px',
  lineHeight: '19px',
  textTransform: 'uppercase',
  borderBottom: '1px solid #181C28',
  '&:hover': {
    cursor: 'pointer'
  },
}))

const ExpandMenuIcon = styled(ExpandIcon, { shouldForwardProp: (prop) => prop !== "isOpen" })<{ isOpen?: boolean; }>(({ isOpen }) => ({
  transform: isOpen ? 'rotate(180deg)' : 'rotate(360deg)',
  fill: 'rgba(203, 203, 203, 0.5)',
  position: 'absolute',
  right: '20px',
  top: '20px',
  fontSize: '40px'
}))

const OverallChartContainer = styled('div')(({ theme }) => ({
  [maxWidth768pxQuery]: {
    padding: '22px 18px',
  },
}))

const OverallTitle = styled(Typography)(() => ({
  fontFamily: 'Arial',
  fontStyle: 'normal',
  fontWeight: '400',
  fontSize: '32px',
  lineHeight: '37.5px',
  marginBottom: '18px'
}))

const ChartPlaceholder = styled('div')(() => ({
  width: '100%',
  height: '310px',
  color: '#000'
}))

const StockOptionsContainer = styled('div')(({ theme }) => ({
  width: 'calc(100% - 60px)',
  background: '#fff',
  boxShadow: '0px 4px 10px rgba(0, 0, 0, 0.25)',
  marginTop: '18px',
  padding: '25px 30px',
  color: '#000',
  overflowX: 'auto',

  [maxWidth768pxQuery]: {
    marginTop: '44px',
    boxShadow: 'none'
  }
}))

const StockOptionsTitle = styled(Typography)(() => ({
  fontFamily: 'Arial',
  fontStyle: 'normal',
  fontWeight: '400',
  fontSize: '32px',
  lineHeight: '38px',
}))

const Table = styled('table')(({ theme }) => ({
  width: '100%',
  fontFamily: 'Arial',
  fontStyle: 'normal',
  fontWeight: '400',
  fontSize: '13px',
  overflowX: 'scroll',
  'th': {
    fontWeight: '700'
  },
  'thead, tr': {
    borderBottom: '1px solid #EBEBEB',
    overflowX: 'hidden',
  },
  'td, th': {
    textAlign: 'left',
    padding: '18px 0'
  },
  'tr:hover': {
    cursor: 'pointer'
  },
  'tr:nth-child(odd) td:first-child': {
    padding: '0',
    lineHeight: '100%',
    verticalAlign: 'middle',
    textAlign: 'center',
  },

  [minWidth768pxQuery]: {
    // Google row
    'tbody tr:nth-child(5)': {
      pointerEvents: 'none'
    },

    // Microsoft row
    'tbody tr:nth-child(8) div': {
      pointerEvents: 'none'
    },

    // Tesla row
    'tbody tr:nth-child(4) div': {
      display: 'flex',
      flexDirection: 'row-reverse',
      'button:first-child': {
        borderLeft: 'none',
        borderRight: '1px solid',
        borderTopRightRadius: '4px',
        borderBottomRightRadius: '4px',
        borderTopLeftRadius: '0',
        borderBottomLeftRadius: '0',
      },
      'button:last-child': {
        borderRight: 'none',
        borderLeft: '1px solid',
        borderTopLeftRadius: '4px',
        borderBottomLeftRadius: '4px',
        borderTopRightRadius: '0',
        borderBottomRightRadius: '0',
      }
    }
  },
  
  [maxWidth768pxQuery]: {
    width: '300%',
    
    // Tesla row
    'tbody tr:nth-child(4) button': {
      padding: '10px',
      fontSize: '10px'
    }
  }
}))

const StockExpandedContainer = styled('tr', { shouldForwardProp: (prop) => prop !== "isOpen" })<{ isOpen?: boolean; }>(({ isOpen }) => ({
  visibility: isOpen ? 'visible' : 'collapse',
  position: 'relative'
}))

const StyledTr = styled('tr', { shouldForwardProp: (prop) => prop !== "isOpen" })<{ isOpen?: boolean; }>(({ isOpen }) => ({
  borderBottom: isOpen ? 'none !important' : '1px solid #EBEBEB !important'
}))

const ExpandTableIcon = styled(ExpandIcon, { shouldForwardProp: (prop) => prop !== "isOpen" })<{ isOpen?: boolean; }>(({ isOpen }) => ({
  transform: isOpen ? 'rotate(180deg)' : 'rotate(360deg)',
  fill: '#000',
  position: 'absolute',
  right: '15px',
  top: '10px',
  fontSize: '30px'
}))

const StockImage = styled('img')(() => ({
  width: '25px',
  height: '25px',
}))

const MenuIconStyled = styled(MenuIcon)(({ theme }) => ({
  cursor: 'pointer',
  fontSize: '45px',

  [minWidth768pxQuery]: {
    display: 'none'
  }
}))

const CloseIconStyled = styled(CloseIcon)(({ theme }) => ({
  cursor: 'pointer',
  fontSize: '45px',
  position: 'absolute',
  right: '20px',
  top: '20px',
  fill: 'rgba(203, 203, 203, 0.5)',

  [minWidth768pxQuery]: {
    display: 'none'
  }
}))

const StockOptionsTitleContainer = styled('div')(({ theme }) => ({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',

  [maxWidth576pxQuery]: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    gap: '16px',
    marginBottom: '16px'
  },

  '@media (min-width: 768px) and (max-width: 970px)': {
    flexDirection: 'column',
    alignItems: 'flex-start',
    gap: '16px',
    marginBottom: '16px'
  }
}))

const GreenEvaluation = styled('span')(() => ({
  color: '#00E031'
}))

const RedEvaluation = styled('span')(() => ({
  color: '#EF0000'
}))

const SearchIconStyled = styled(SearchIcon)(() => ({}))

const SearchInput = styled('input')(({ theme }) => ({
  border: 'none',
  borderBottom: '1px solid #B0B0B0',
  outline: 'none',
  padding: '10px 0',
  width: '246px',

  [maxWidth768pxQuery]: {
    pointerEvents: 'none'
  }
}))

const ChartViewGroup = styled('div', { shouldForwardProp: (prop) => prop !== "viewType" })<{ viewType: string; }>(({ viewType }) => ({
  position: 'absolute',
  right: '15px',
  bottom: '17px',
  'button': {
    background: '#FFFFFF', 
    borderWidth: '1px 1px 1px 1px', 
    borderStyle: 'solid', 
    borderColor: '#9D9D9D', 
    padding: '5px 13px',
    cursor: 'pointer',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: '400',
    fontSize: '10px',
    lineHeight: '12px',
    color: '#000000',

  },
  'button:first-child': {
    borderRight: 'none',
    borderTopLeftRadius: '4px',
    borderBottomLeftRadius: '4px',
    background: viewType === 'day' ? '#D9D9D9' : '#fff'
  },
  'button:last-child': {
    borderLeft: 'none',
    borderTopRightRadius: '4px',
    borderBottomRightRadius: '4px',
    background: viewType === 'week' ? '#D9D9D9' : '#fff'
  }
}))

const theme = createTheme({
  palette: {
    primary: {
      main: '#000'
    },
  },
});

const App = () => {
  const [openedMenu, setOpenedMenu] = useState<string>('');
  const [openedStock, setOpenedStock] = useState<string>('');
  const [isSideMenuOpen, setSideMenuOpen] = useState<boolean>(false);
  const [filter, setFilter] = useState<string>('');
  const [chartViewType, setChartViewType] = useState<string>('day');

  const stockData = [appleData, teslaData, googleData, microsoftData]

  const handleMenuItemClick = (menuItem: string) => {
    setOpenedMenu(menuItem === openedMenu ? '' : menuItem)
  }

  const handleStockItemClick = (stockItem: string) => {
    setOpenedStock(stockItem === openedStock ? '' : stockItem)
  }

  const handleLogout = () => {
    const auth: any = {};
    auth.logout();
  }

  const handleUserProfileClick = () => {
    fetch('http://localhost:3009/my-profile')
      .then((response) => response.json())
      .then((data) => {
        console.log(data)
      })
  }

  const handleOverallChartClick = () => {
    const chart: any = {};
    chart.callAction();
  }

  useEffect(() => {
    fetch('http://localhost:3009/data/airbnb.json')
      .then((response) => response.json())
      .then((data) => {
        stockData.push(data);
      })
  }, [])

  return (
    <CacheProvider value={cache}>
      <ThemeProvider theme={theme}>
        <AppContainer>
          <SideMenu isOpen={isSideMenuOpen}>
            <CloseIconStyled onClick={() => setSideMenuOpen(false)} />

            <UserContainer onClick={handleUserProfileClick}>          
              <UserImage />
              <div>
                <UserName>John Reginald Doe</UserName>
                <UserTitle>Investor</UserTitle>
              </div>
            </UserContainer>

            <MenuItemList>
              <MenuItem onClick={() => handleMenuItemClick('stock-browser')}>
                <ScreenSearchDesktopIcon /> <span>Stock Browser</span> <ExpandMenuIcon isOpen={openedMenu === 'stock-browser'} />
              </MenuItem>

              <SubMenuItemsList isOpen={openedMenu === 'stock-browser'}>
                <SubMenuItem>By Category</SubMenuItem>
                <SubMenuItem>By Price</SubMenuItem>
                <SubMenuItem>By Trend</SubMenuItem>
              </SubMenuItemsList>
              
              <MenuItem onClick={() => handleMenuItemClick('configuration')}>
                <SettingsIcon /> <span>Configuration</span> <ExpandMenuIcon isOpen={openedMenu === 'configuration'} />
              </MenuItem>

              <SubMenuItemsList isOpen={openedMenu === 'configuration'}>
                <SubMenuItem>My Account</SubMenuItem>
                <SubMenuItem>My Wallet</SubMenuItem>
                <SubMenuItem>My Profile</SubMenuItem>
              </SubMenuItemsList>
              
              <MenuItem onClick={() => handleMenuItemClick('support')}>
                <SupportAgentIcon /> <span>Suport</span>  <ExpandMenuIcon isOpen={openedMenu === 'support'} />
              </MenuItem>

              <SubMenuItemsList isOpen={openedMenu === 'support'}>
                <SubMenuItem>Account</SubMenuItem>
                <SubMenuItem>Negociation</SubMenuItem>
                <SubMenuItem>Stocks</SubMenuItem>
              </SubMenuItemsList>
            </MenuItemList>

            <LogoutSection onClick={handleLogout}>
              <LogoutIcon sx={{ fontSize: '15px' }} /> <p><span>Lo</span><span style={{ marginLeft: '7px' }}>gout</span></p>
            </LogoutSection>
          </SideMenu>

          <MainContent>
            <Background />
            <Container>
              <OverallChartContainer>
                <MenuIconStyled onClick={() => setSideMenuOpen(true)}>open</MenuIconStyled>

                <OverallTitle>Overal Comparison</OverallTitle>
                <ChartPlaceholder>
                  <Line onClick={handleOverallChartClick} options={overallChartOptions} data={overallChartData} style={{ cursor: 'pointer' }} />
                </ChartPlaceholder>
              </OverallChartContainer>

              <StockOptionsContainer>
                <StockOptionsTitleContainer>
                  <StockOptionsTitle>Stock Options</StockOptionsTitle>

                  <Box sx={{ display: 'flex', alignItems: 'flex-end', gap: '4px' }}>
                    <SearchIconStyled />
                    <SearchInput placeholder="Search stocks here" onChange={({ target }) => setFilter(target.value)} />
                  </Box>
                </StockOptionsTitleContainer>

                <Table>
                  <thead>
                    <tr>
                      <th></th>
                      <th>Stock</th>
                      <th>Price</th>
                      <th>Evaluation</th>
                      <th>Volume</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {stockData.filter(stock => stock.name.includes(filter)).map((stockAction) => (
                      <React.Fragment key={`${stockAction.name}-row`}>
                        <StyledTr onClick={() => handleStockItemClick(stockAction.name)} isOpen={openedStock === stockAction.name}>
                          <td><StockImage src={require(`./img/${stockAction.image}`)} /></td>
                          <td>{stockAction.name}</td>
                          <td>$ {stockAction.price}</td>
                          <td>{stockAction.evaluation.includes('+') 
                            ? <GreenEvaluation>{stockAction.evaluation}</GreenEvaluation>
                            : <RedEvaluation>{stockAction.evaluation}</RedEvaluation>}</td>
                          <td>{stockAction.volume}</td>
                          <td style={{ position: 'relative' }}><ExpandTableIcon /></td>
                        </StyledTr>
                        <StockExpandedContainer isOpen={openedStock === stockAction.name}>
                          <td colSpan={6}>
                            <Line options={stockChartOptions} data={{
                                ...(chartViewType === 'day' ? stockChartDayData : stockChartWeekData),
                                datasets: [
                                  {
                                    ...(chartViewType === 'day' ? stockChartDayData : stockChartWeekData).datasets[0],
                                    data: stockAction.data[`${chartViewType}s`],
                                    label: `Stock evaluation in dollars (${stockAction.symbol})`
                                  }
                                ]
                              }} 
                            />

                            <ChartViewGroup viewType={chartViewType}>
                              <button onClick={() => setChartViewType('day')}>Day</button>
                              <button onClick={() => setChartViewType('week')}>Week</button>
                            </ChartViewGroup>
                          </td>
                        </StockExpandedContainer>
                      </React.Fragment>
                    ))}
                  </tbody>
                </Table>
              </StockOptionsContainer>
            </Container>
          </MainContent>
        </AppContainer>
      </ThemeProvider>
    </CacheProvider>
  )
}

export default App;